# Curso Comandos Linux
- **Autor**: ProyectoLibreX.
- **Idioma**: Español.
- **Temática**: Comandos GNU/Linux & MacOS.
- **Plataforma**: YouTube (ProyectoLibreX).
- **Estado**: En curso. 

## Introducción:
En este repositorio, encontrarás todos los recursos que voy a ir mostrandote en el curso de "Comandos Linux" de mi canal de YouTube (ProyectoLibreX).
Disfruta, aprende, comparte con quien quieras y siéntete libre de aportar tu feedback.

## ¿Quieres contribuir?
Este es un curso hecho por y para la comunidad. Toda colaboración es sumamente valiosa y será bienvenida tras ser revisada.
Todo aquel que colabore, si lo desea, será reconocido. Si necesitas ponerte en contacto conmigo, por favor, hazlo mediante mi canal de YouTube (ProyectoLibreX). 

## Licencia CC BY-NC-SA:
Puedes modificar, adaptar, y construir sobre este proyecto siempre que: 
1) Sea de manera no-comercial. 
2) Des credito a su autor. 
3) Licencies tu proyecto derivado bajo la misma licencia (CC BY-NC-SA).
